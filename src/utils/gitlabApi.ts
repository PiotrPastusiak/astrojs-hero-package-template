import token from '../data/token.json';

async function getReleasesDataFromGitlab(privateToken: HeadersInit, url: URL) {

    const options: RequestInit = { headers: privateToken };

    return fetch(url, options)
        .then((response) => response.json())
        .then((data) => {
            return data;
        })
        .catch((error) => {
            console.error("Problem with fetching Gitlab Releases API:", error);
        });
}

function parseReleasesApiResp(releasesData: any) {
    console.log("Found releases: ", releasesData.length);
    const latestRelease = releasesData[0];
    const packageLink = latestRelease.assets.links.find(
        (element: any) => element.link_type == "package"
    );

    const obj = {
        name: latestRelease.tag_name,
        tag: latestRelease.tag_name,
        url: packageLink.url,
    };

    return obj;
}

export async function gatherReleaseInfo() {

    const authHeader = token[0];
    const privateToken = token[1];

    const apiUrl = new URL("https://gitlab.com/api/v4/projects/41218551/releases");
    let requestHeaders: HeadersInit = new Headers();
    requestHeaders.set(authHeader, privateToken);

    console.log(requestHeaders);
    const releasesData = await getReleasesDataFromGitlab(requestHeaders, apiUrl);
    return parseReleasesApiResp(releasesData);
}
