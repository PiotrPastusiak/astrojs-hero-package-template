import { defineConfig } from 'astro/config';

// https://astro.build/config
export default defineConfig({
    base: '/astrojs-hero-package-template',
    outDir: 'public',
    publicDir: 'static',
    markdown: {
        shikiConfig: {
            theme: 'one-dark-pro',
            langs: [],
            wrap: true,
        },
    },
});
